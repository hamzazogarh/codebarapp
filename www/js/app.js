var app = angular.module('starter', ['ionic','ngCordova','chart.js', 'angularMoment']);

app
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.hide();
    }
  });
})


// FActory
.factory('wpFactory', function ($http, $q) {
  var urlApi = 'http://13.81.49.5/clients/codebar/wp-json/wp/v2/';


  // Get Check code Bar
  function checkCodeBar(codebar) {
    return ($http.get(urlApi + 'code_barres?code="' + codebar + '"')
        .then(handleSuccess, handleError));
  }
  // Get Product
  function getProduct(codebarID) {
    return ($http.get(urlApi + 'produits?codebar=' + codebarID)
        .then(handleSuccess, handleError));
  }

  // HandleSucces Fn's
  function handleSuccess(response) {
    return response.data;
  }
  function handleError(response) {
    if (!angular.isObject(response.data) || !response.data.message) {
      return($q.reject("An unknown error occurred."));
    }
    return($q.reject(response.data.message));
  }

  return({
    checkCodeBar: checkCodeBar,
    getProduct: getProduct
  });
})

.factory('paysCode', function ($http, $q) {
    pays = [
      {
          code:"000,060",
          pays:"États-Unis"
      },
      {
          code:"300,379",
          pays:"France"
      },
      {
          code:"380,380",
          pays:"Bulgarie"
      },
      {
          code:"383,383",
          pays:"Slovénie"
      },
      {
          code:"385,385",
          pays:"Croatie"
      },
      {
          code:"387,387",
          pays:"Boznie-Herzégovine"
      },
      {
          code:"400,440",
          pays:"Allemagne"
      },
      {
          code:"450,459",
          pays:"Japon"
      },
      {
          code:"490,499",
          pays:"Japon"
      },
      {
          code:"460,469",
          pays:"Russie"
      },
      {
          code:"470,470",
          pays:"Kirgistan"
      },
      {
          code:"471,471",
          pays:"Taïwan"
      },
      {
          code:"474,474",
          pays:"Estonie"
      },
      {
          code:"475,475",
          pays:"Lettonie"
      },
      {
          code:"476,476",
          pays:"Azerbaijan"
      },
      {
          code:"477,477",
          pays:"Lituanie"
      },
      {
          code:"478,478",
          pays:"Ouzebekistan"
      },
      {
          code:"479,479",
          pays:"Sri Lanka"
      },
      {
          code:"480,480",
          pays:"Philippines"
      },
      {
          code:"481,481",
          pays:"Biélorussie"
      },
      {
          code:"482,482",
          pays:"Ukraine"
      },
      {
          code:"484,484",
          pays:"Moldavie"
      },
      {
          code:"485,485",
          pays:"Arménie"
      },
      {
          code:"486,486",
          pays:"Géorgie"
      },
      {
          code:"487,487",
          pays:"Kazakhstan"
      },
      {
          code:"489,489",
          pays:"Hong Kong"
      },
      {
          code:"500,509",
          pays:"Royaume-Uni"
      },
      {
          code:"520,520",
          pays:"Grèce"
      },
      {
          code:"528,528",
          pays:"Liban"
      },
      {
          code:"529,529",
          pays:"Chypre"
      },
      {
          code:"530,530",
          pays:"Albanie"
      },
      {
          code:"531,531",
          pays:"Macédoine"
      },
      {
          code:"535,535",
          pays:"Malte"
      },
      {
          code:"539,539",
          pays:"Irlande"
      },
      {
          code:"540,549",
          pays:"Belgique & Luxembourg"
      },
      {
          code:"560,560",
          pays:"Portugal"
      },
      {
          code:"569,569",
          pays:"Islande"
      },
      {
          code:"570,579",
          pays:"Danemark"
      },
      {
          code:"590,590",
          pays:"Pologne"
      },
      {
          code:"594,594",
          pays:"Roumanie"
      },
      {
          code:"599,599",
          pays:"Hongrie"
      },
      {
          code:"600,601",
          pays:"Afrique du Sud"
      },
      {
          code:"603,603",
          pays:"Ghana"
      },
      {
          code:"608,608",
          pays:"Bahrein"
      },
      {
          code:"609,609",
          pays:"Île Maurice"
      },
      {
          code:"611,611",
          pays:"Maroc"
      },
      {
          code:"613,613",
          pays:"Algérie"
      },
      {
          code:"616,616",
          pays:"Kenya"
      },
      {
          code:"618,618",
          pays:"Côte d’Ivoire"
      },
      {
          code:"619,619",
          pays:"Tunisie"
      },
      {
          code:"621,621",
          pays:"Syrie"
      },
      {
          code:"622,622",
          pays:"Égypte"
      },
      {
          code:"624,624",
          pays:"Libye"
      },
      {
          code:"625,625",
          pays:"Jordanie"
      },
      {
          code:"626,626",
          pays:"Iran"
      },
      {
          code:"627,627",
          pays:"Koweit"
      },
      {
          code:"628,628",
          pays:"Arabie Saoudite"
      },
      {
          code:"629,629",
          pays:"Émirats Arabes Unis"
      },
      {
          code:"640,649",
          pays:"Finlande"
      },
      {
          code:"690,695",
          pays:"Chine"
      },
      {
          code:"700,709",
          pays:"Norvège"
      },
      {
          code:"729,729",
          pays:"Israël"
      },
      {
          code:"730,739",
          pays:"Suède"
      },
      {
          code:"740,740",
          pays:"Guatemala"
      },
      {
          code:"741,741",
          pays:"Salvador"
      },
      {
          code:"742,742",
          pays:"Honduras"
      },
      {
          code:"743,743",
          pays:"Nicaragua"
      },
      {
          code:"744,744",
          pays:"Costa Rica"
      },
      {
          code:"745,745",
          pays:"Panama"
      },
      {
          code:"746,746",
          pays:"République Dominicaine"
      },
      {
          code:"750,750",
          pays:"Mexique"
      },
      {
          code:"754,755",
          pays:"Canada"
      },
      {
          code:"759,759",
          pays:"Venezuela"
      },
      {
          code:"760,769",
          pays:"Suisse"
      },
      {
          code:"770,770",
          pays:"Colombie"
      },
      {
          code:"773,773",
          pays:"Uruguay"
      },
      {
          code:"775,775",
          pays:"Pérou"
      },
      {
          code:"777,777",
          pays:"Bolivie"
      },
      {
          code:"779,779",
          pays:"Argentine"
      },
      {
          code:"780,780",
          pays:"Chili"
      },
      {
          code:"784,784",
          pays:"Paraguay"
      },
      {
          code:"786,786",
          pays:"Équateur"
      },
      {
          code:"789,790",
          pays:"Brésil"
      },
      {
          code:"800,839",
          pays:"Italie"
      },
      {
          code:"840,849",
          pays:"Espagne"
      },
      {
          code:"850,850",
          pays:"Cuba"
      },
      {
          code:"858,858",
          pays:"Slovaquie"
      },
      {
          code:"859,859",
          pays:"République tchèque"
      },
      {
          code:"860,860",
          pays:"Serbie Monténégro"
      },
      {
          code:"865,865",
          pays:"Mogolie"
      },
      {
          code:"867,867",
          pays:"Corée du Nord"
      },
      {
          code:"869,869",
          pays:"Turquie"
      },
      {
          code:"870,879",
          pays:"Pays-Bas"
      },
      {
          code:"880,880",
          pays:"Corée du Sud"
      },
      {
          code:"884,884",
          pays:"Cambodge"
      },
      {
          code:"885,885",
          pays:"Thaïlande"
      },
      {
          code:"888,888",
          pays:"Singapour"
      },
      {
          code:"890,890",
          pays:"Inde"
      },
      {
          code:"893,893",
          pays:"Vietnam"
      },
      {
          code:"899,899",
          pays:"Indonésie"
      },
      {
          code:"900,919",
          pays:"Autriche"
      },
      {
          code:"930,939",
          pays:"Australie"
      },
      {
          code:"940,949",
          pays:"Nouvelle-Zélande"
      },
      {
          code:"955,955",
          pays:"Malaisie"
      },
      {
          code:"958,958",
          pays:"Macao"
      }
    ];

    return pays;
})

.config(function($stateProvider, $urlRouterProvider,ChartJsProvider){
    // ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });

    $stateProvider
        .state('home', {
            url: '/home',
            controller: 'HomeCtrl',
            templateUrl: 'views/home/home.html'
        })

        .state('tabs', {
            url: '/tab',
            controller: 'TabsCtrl',
            templateUrl: 'views/tabs.html'
        })

        .state('tabs.scanner', {
            url: '/scanner',
            views: {
                'scanner-tab': {
                    templateUrl: 'views/scanner/index.html',
                    controller: 'ScannerTabCtrl'
                }
            }
        })
        .state('tabs.statistiques', {
            url: '/statistiques',
            views: {
                'statistiques-tab': {
                    templateUrl: 'views/statistiques/index.html',
                    controller: 'StatistiquesTabCtrl'
                }
            }
        })
        .state('tabs.classements', {
            url: '/classements',
            views: {
                'classements-tab': {
                    templateUrl: 'views/classements/index.html',
                    controller: 'ClassementsTabCtrl'
                }
            }
        })
        .state('tabs.historiques', {
            url: '/historiques',
            views: {
                'historiques-tab': {
                    templateUrl: 'views/historiques/index.html',
                    controller: 'HistoriquesTabCtrl'
                }
            }
        })
        .state('tabs.produits', {
            url: '/produits/:id',
            views: {
                'produits-tab': {
                    templateUrl: 'views/produits/index.html',
                    controller: 'ProduitsTabCtrl'
                }
            }
        });

        $urlRouterProvider.otherwise('/home');

})

.controller('TabsCtrl', function($scope, $ionicSideMenuDelegate) {

    $scope.openMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };

})

;