app.controller('ScannerTabCtrl', function($scope, $ionicSideMenuDelegate,$state, $cordovaBarcodeScanner,$ionicPlatform,$http, wpFactory,paysCode) {
    $scope.openMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    }



// angular.forEach(paysCode,function (value,index) {
//
//     var barcodeData = {
//         "format":"UPC_E",
//         "cancelled":"0",
//         "text":"02833526"
//     };
//
//     var minCode = parseInt(value.code.split(',')['0']);
//     var firsttThreeCode = parseInt(barcodeData.text.substr(0, 3));
//     var maxCode = parseInt(value.code.split(',')['1']);
//
//     if(minCode <=  firsttThreeCode && firsttThreeCode <= maxCode ){
//         // console.log(barcodeData.text.substr(0, 3));
//         console.log(value.pays);
//     }
// });

    // var barcodeData = {
    //     "format":"UPC_E",
    //     "cancelled":"0",
    //     "text":"6111128000460"
    // };
    //
    //
    // wpFactory.checkCodeBar(barcodeData.text).then(function (succ) {
    //     $scope.readingData = succ;
    //
    //     if($scope.readingData['0'].id){
    //         console.log($scope.readingData);
    //
    //         $state.go('tabs.produits', {
    //             id: $scope.readingData.id //selectedItem and id is defined
    //         });
    //     }else {
    //         console.log('add New');
    //     }
    //
    // }, function error(err) {
    //     console.log('Errror: ', err);
    // });

    $scope.scanResultJson = {};
    $scope.readingData = [];

    $scope.scan = function(){
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner.scan().then(function(barcodeData) {
                // alert(JSON.stringify(barcodeData));
                // alert(barcodeData.text);
                $scope.scanResultJson = JSON.stringify(barcodeData);


                wpFactory.checkCodeBar($scope.scanResultJson.text).then(function (succ) {
                    $scope.readingData = succ;

                    if($scope.readingData['0'].id){
                        console.log($scope.readingData);

                        $state.go('tabs.produits', {
                            id: $scope.readingData.id //selectedItem and id is defined
                        });
                    }else {
                        console.log('add New');
                    }

                }, function error(err) {
                    console.log('Errror: ', err);
                });

            }, function(error) {
                alert(JSON.stringify(error));
            });
        });
    };

});