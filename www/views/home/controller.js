app.controller('HomeCtrl', function($scope, $rootScope, $ionicSideMenuDelegate,$state, $cordovaBarcodeScanner,$ionicPlatform,$http, wpFactory,paysCode) {
    $rootScope.hiddenHeader = true;

    $scope.scanResultJson = {};
    $scope.readingData = [];

    $scope.scan = function(){
        $ionicPlatform.ready(function() {
            $cordovaBarcodeScanner.scan().then(function(barcodeData) {
                // alert(JSON.stringify(barcodeData));
                // alert(barcodeData.text);
                $scope.scanResultJson = JSON.stringify(barcodeData);


                wpFactory.checkCodeBar($scope.scanResultJson.text).then(function (succ) {
                    $scope.readingData = succ;

                    if($scope.readingData['0'].id){
                        console.log($scope.readingData);

                        $state.go('tabs.produits', {
                            id: $scope.readingData.id //selectedItem and id is defined
                        });
                    }else {
                        console.log('add New');
                    }

                }, function error(err) {
                    console.log('Errror: ', err);
                });

            }, function(error) {
                alert(JSON.stringify(error));
            });
        });
    };

});