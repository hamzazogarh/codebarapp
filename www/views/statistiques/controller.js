app.controller('StatistiquesTabCtrl', function($scope, $ionicSideMenuDelegate) {
    $scope.openMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };

    // ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });

    $scope.labels = ["Excellent", "Bon", "Médiocre", "Mauvais"];
    $scope.data = [10, 15, 6, 5];
    // $scope.colors = [ '#0dc24a', '#07e370', '#e59600', '#fc123f'];
    $scope.override = { borderColor: ['rgba(255,255,255,0)', 'rgba(255,255,255,0)'] };
    $scope.colors = [{
        backgroundColor: '#0dc24a',
        borderColor: 'rgba(255,255,255,0)',
        pointBackgroundColor: '#0dc24a',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: 'rgb(252, 18, 63)',
        pointHoverBorderColor: 'rgba(255,255,255,0)'
    },{
        backgroundColor: '#07e370',
        borderColor: 'rgba(255,255,255,0)',
        pointBackgroundColor: '#07e370',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: 'rgba(255,255,255,0)',
        pointHoverBorderColor: 'rgba(255,255,255,0)'
    },{
        backgroundColor: '#e59600',
        borderColor: 'rgba(255,255,255,0)',
        pointBackgroundColor: '#e59600',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: 'rgba(255,255,255,0)',
        pointHoverBorderColor: 'rgba(255,255,255,0)'
    },{
        backgroundColor: '#fc123f',
        borderColor: 'rgba(255,255,255,0)',
        pointBackgroundColor: '#fc123f',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: 'rgba(255,255,255,0)',
        pointHoverBorderColor: 'rgba(255,255,255,0)'
    }
    ];
});